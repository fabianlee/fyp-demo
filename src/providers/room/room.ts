export class Room {
    constructor(
        private room_name: string,
        private about: string,
        private booking_rule: any[],
        private opening_hour: {},
        private restricted_booking_time: any[],
        private advance_booking: any[],
        private equipment: any[],
        private groups: string[],
        private id: string
    ) {}
}