import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http"
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

import { Room } from './room';
import { Config } from "../config";

@Injectable()
export class RoomService {

  constructor(private http: Http) {}

  // Get the entire room list and map them to an array of Room objects
  load() {
    return this.http.get(Config.apiUrl + "rooms" + "?access_token=" + Config.token)
      .map(res => res.json())
      .map(data => {
        let roomList = [];
        data.forEach((room) => {
          roomList.push(new Room(
            room.room_name,
            room.about,
            room.booking_rule,
            room.opening_hour,
            room.restricted_booking_time,
            room.advance_booking,
            room.equipment,
            room.groups,
            room.id
          ));
        });
        return roomList;
      })
      .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
}
