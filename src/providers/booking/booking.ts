export class Booking {
  event: {};
  start_time: string;
  end_time: string;
  userId: string;
  roomId: string
}