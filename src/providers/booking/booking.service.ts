import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http"
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";

import { Booking } from './booking';
import { Config } from "../config";

@Injectable()
export class BookingService {

  constructor(private http: Http) {}

  bookNow(booking: Booking) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
  
    return this.http.post(
      Config.apiUrl + "rooms/"+ booking.roomId + "/bookings" + "?access_token=" + Config.token,
      JSON.stringify({ 
        event: booking.event,
        start_time: booking.start_time,
        end_time: booking.end_time,
        userId: Config.userID,
        roomId: booking.roomId 
      }),
      { headers: headers }
    )
    .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
}