import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http"
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import 'rxjs/add/observable/throw';

import { User } from './user';
import { Config } from "../config";

@Injectable()
export class UserService {

    constructor(private http: Http) {}

    login(user: User) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
      
        return this.http.post(
          Config.apiUrl + "ustUsers/login",
          JSON.stringify({
            username: user.email,
            password: user.password,
          }),
          { headers: headers }
        )
        .map(response => response.json())
        .do(data => {
          Config.token = data.id;
          Config.userID = data.userId;
        })
        .catch(this.handleErrors);
    }

      handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));
        return Observable.throw(error);
    }
}