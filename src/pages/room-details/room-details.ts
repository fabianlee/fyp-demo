import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Room } from '../../providers/room/room';
import { CalendarPage } from '../calendar/calendar';
// import { BookingDetailsPage } from '../booking-details/booking-details';

/**
 * Generated class for the RoomDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-room-details',
  templateUrl: 'room-details.html',
})
export class RoomDetailsPage {
  
  room: Room;
  timeslots = ["10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00"];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.room = this.navParams.data;
    console.log(this.room);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomDetailsPage');
  }

  checkAvailabilty(room: any) {
    this.navCtrl.push(CalendarPage, {roomID: room.id});
  }

  // goToBookingDetails (date: any, slot:any) {
  //   this.navCtrl.push(BookingDetailsPage, { bookingDate: date, bookingSlot: slot })
  // }

}
