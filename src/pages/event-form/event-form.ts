import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BookingService } from '../../providers/booking/booking.service';
import { Booking } from '../../providers/booking/booking';


/**
 * Generated class for the EventFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-form',
  templateUrl: 'event-form.html',
  providers: [BookingService]
})



export class EventFormPage {
  event: any;
  booking: Booking;

  constructor(public navCtrl: NavController, public navParams: NavParams, private bookingService: BookingService) {
    this.booking = new Booking();
    this.booking.event = this.event;
    this.booking.start_time = this.navParams.data.start;
    this.booking.end_time = this.navParams.data.end;
    this.booking.roomId = this.navParams.data.roomID;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventFormPage');
  }

  book() {
    this.bookingService.bookNow(this.booking)
    .subscribe(
      () => console.log("OK"),
      (error) => console.log("Fail")
    );
    this.navCtrl.popToRoot();
  }

}
