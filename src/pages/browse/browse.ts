import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RoomDetailsPage } from '../room-details/room-details';

import { RoomService } from '../../providers/room/room.service';
import { Room } from '../../providers/room/room';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

/**
 * Generated class for the BrowsePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html',
  providers: [RoomService]
})
export class BrowsePage {

  roomList: Array<Room> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private roomService: RoomService, public nativePageTransitions: NativePageTransitions) {
  }

  ionViewDidLoad() {
    this.roomService.load()
    .subscribe(loadedRooms => {
        loadedRooms.forEach((roomObject) => {
          this.roomList.push(roomObject)
        });
      });
      console.log(this.roomList);
  }

  viewDetails (room: Room, slideBtn) {
    let options: NativeTransitionOptions = {
      "duration"       :  600, // in milliseconds (ms), default 400
      "iosdelay"       :   50, // ms to wait for the iOS webview to update before animation kicks in, default 60
      "androiddelay"   :  100
    }
    this.nativePageTransitions.fade(options);
    slideBtn.close();
    this.navCtrl.push(RoomDetailsPage, room);
  }
}
