import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { User } from '../../providers/user/user'
import { UserService } from '../../providers/user/user.service'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  providers: [UserService],
  templateUrl: 'login.html',
})
export class LoginPage {

  public backgroundImage = '../../assets/imgs/login-bg.jpg';
  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController , private userService: UserService) {
    this.user = new User();
    this.user.email = "admin";
    this.user.password = "123456";
  }

  ionViewDidLoad() {}

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Sorry we cannot find your account.',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  login() {
    this.userService.login(this.user)
    .subscribe(
      () => this.navCtrl.push(TabsPage),
      (error) => this.presentAlert()
    );
  }

}
