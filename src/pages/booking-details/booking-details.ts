import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventFormPage } from '../event-form/event-form';

/**
 * Generated class for the BookingDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking-details',
  templateUrl: 'booking-details.html',
})
export class BookingDetailsPage {

  selectedDate: Date;
  selectedSlot = '';
  selectedRoomID = '';
  splitTime = [];
  startTime = '';
  endTime = '';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selectedDate = this.navParams.data.bookingDate._d;
    this.selectedSlot = this.navParams.data.bookingSlot;
    this.selectedRoomID = this.navParams.data.roomID;
  }

  ionViewDidLoad() {
    this.parseStartAndEnd();
    console.log('ionViewDidLoad BookingDetailsPage');
    console.log(this.startTime);
    console.log(this.endTime);
    console.log(this.selectedRoomID);
  }

  parseStartAndEnd() {
    this.splitTime = this.selectedSlot.split(':');
    this.selectedDate.setHours(parseInt(this.splitTime[0]));
    this.selectedDate.setMinutes(parseInt(this.splitTime[1]));
    this.startTime = this.selectedDate.toISOString();
    this.selectedDate.setMinutes(parseInt(this.splitTime[1])+59);
    this.endTime = this.selectedDate.toISOString();
  }

  goToEventForm() {
    this.navCtrl.push(EventFormPage, { start: this.startTime, end: this.endTime, roomID: this.selectedRoomID });
  }

}
