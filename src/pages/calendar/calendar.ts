import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CalendarComponentOptions } from 'ion2-calendar';
import { BookingDetailsPage } from '../booking-details/booking-details';

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  
  currentRoomID = '';
  timeslots = ["10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00"];
  dateMulti: string[];
  type: 'js-date'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsMulti: CalendarComponentOptions = {
    pickMode: 'multi'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentRoomID = this.navParams.data.roomID;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
    console.log(this.currentRoomID);
  }

  goToBookingDetails (date: any, slot:any) {
    this.navCtrl.push(BookingDetailsPage, { bookingDate: date, bookingSlot: slot, roomID: this.currentRoomID })
  }

}
