import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions'
import { CalendarModule } from "ion2-calendar";

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { BrowsePage } from '../pages/browse/browse';
import { InboxPage } from '../pages/inbox/inbox';
import { AccountPage } from '../pages/account/account';
import { BookingsPage } from '../pages/bookings/bookings';
import { RoomDetailsPage } from '../pages/room-details/room-details';
import { TabsPage } from '../pages/tabs/tabs';
import { CalendarPage } from '../pages/calendar/calendar';
import { BookingDetailsPage } from '../pages/booking-details/booking-details';
import { EventFormPage } from '../pages/event-form/event-form';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    BrowsePage,
    TabsPage,
    InboxPage,
    AccountPage,
    BookingsPage,
    RoomDetailsPage,
    CalendarPage,
    BookingDetailsPage,
    EventFormPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CalendarModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      tabsHideOnSubPages: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    BrowsePage,
    TabsPage,
    InboxPage,
    AccountPage,
    BookingsPage,
    RoomDetailsPage,
    CalendarPage,
    BookingDetailsPage,
    EventFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
